var traderModule = (function () {
	var ctrl = {
		vehiculePrice: '#veh_price',
		downPayment: '#down_pay',
		tradeIn: '#trade_in',
		saleTaxe: '#sale_tx',
		interestRate: '#interest_rate',
		months: '#months',
		btnCalculate: '#calculate-btn',
		btnClear: '#clear-btn',
		loadAmount: '#loan_amount',
		loadAmountNoTaxe: '#loan_amount_noTaxe',
		montlyPrice: '#monthly_amount',
		montlyPriceNoTaxe: '#monthly_amount_noTaxe',
	};

	var init = function () {
		clearForm();
	};

	function calculate() {
		var veh_price = parseFloat($(ctrl.vehiculePrice).val());
		var down_pay = parseFloat($(ctrl.downPayment).val());
		var trade_in = parseFloat($(ctrl.tradeIn).val());
		var sale_tx = parseFloat($(ctrl.saleTaxe).val());
		var interest_rate = parseFloat($(ctrl.interestRate).val());
		var nbMonth = parseFloat($(ctrl.months).val());
		var loadAmount = 0;
		var loadAmountNoTaxes = 0;

		if (veh_price == 0) {
			console.log('You must enter a price for the vehicule.');
		} else if (interest_rate == null) {
			console.log('You must enter a interest rate for the vehicule.');
		} else if (interest_rate < 0) {
			console.log('You must enter a interest rate egal or above 0.');
		} else if (nbMonth == 0) {
			console.log('The number of month must be above 1');
		} else {
			if (sale_tx > 0) {
				loadAmount = (veh_price - trade_in - down_pay) * (1 + sale_tx * 0.01);
				loadAmountNoTaxes = veh_price - trade_in - down_pay;
			} else {
				loadAmount = veh_price - down_pay - trade_in;
				loadAmountNoTaxes = veh_price - down_pay - trade_in;
			}
			var payment = 0;
			var paymentNoTaxe = 0;
			if (interest_rate == 0) {
				//no interest
				payment = loadAmount / nbMonth;
				paymentNoTaxe = loadAmountNoTaxes / nbMonth;
			} else {
				payment = calculatePayment(loadAmount, nbMonth, interest_rate);
				paymentNoTaxe = calculatePayment(
					loadAmountNoTaxes,
					nbMonth,
					interest_rate
				);
			}
		}

		$(ctrl.loadAmountNoTaxe).html(loadAmountNoTaxes.toFixed(2));
		$(ctrl.loadAmount).html(loadAmount.toFixed(2));
		$(ctrl.montlyPriceNoTaxe).html(paymentNoTaxe.toFixed(2));
		$(ctrl.montlyPrice).html(payment.toFixed(2));
	}

	function calculatePayment(loadAmount, nbMonth, interest_rate) {
		return calculateInterest(loadAmount, nbMonth, interest_rate);
	}

	var calculateInterest = function (total, nbMonth, rate) {
		rate = rate / 100 / 12;
		var pow = 1;
		for (var j = 0; j < nbMonth; j++) pow = pow * (1 + rate);

		return 0.01 * Math.round((100 * (total * pow * rate)) / (pow - 1));
	};

	function clearForm() {
		$(ctrl.vehiculePrice).val('');
		$(ctrl.downPayment).val('');
		$(ctrl.tradeIn).val('');
		$(ctrl.saleTaxe).val('5');
		$(ctrl.interestRate).val('');
		$(ctrl.months).val('240');
		$(ctrl.loadAmount).html('');
		$(ctrl.montlyPrice).html('');
	}

	return {
		init: init,
		calculate: calculate,
		clearForm: clearForm,
	};
})();
