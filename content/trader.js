var traderModule = (function () {
    var ctrl = {
        vehiculePrice : "#veh_price",
        downPayment: "#down_pay",
        tradeIn: "#trade_in",
        saleTaxe: "#sale_tx",
        interestRate: "#interest_rate",
        year: "#year",
        btnCalculate: "#calculate-btn",
        btnClear: "#clear-btn",
        loadAmount:"#loan_amount",
        montlyPrice:"#monthly_amount"
    }
    
    var init = function(){
        clearForm();
    };

    function calculate() {
        var veh_price = parseFloat($(ctrl.vehiculePrice).val());
        var down_pay = parseFloat($(ctrl.downPayment).val());
        var trade_in = parseFloat($(ctrl.tradeIn).val());
        var sale_tx = parseFloat($(ctrl.saleTaxe).val());
        var interest_rate = parseFloat($(ctrl.interestRate).val());
        var year = parseFloat($(ctrl.year).val());
        var nbPayments = year*12;
        var loadAmount = 0;

        if (veh_price == 0){
            console.log("You must enter a price for the vehicule.")
        }else if (interest_rate == null){
            console.log("You must enter a interest rate for the vehicule.")
        }else if (interest_rate < 0){
            console.log("You must enter a interest rate egal or above 0.")
        }else if (nbPayments == 0){
            console.log("The year must be above 1")
        }else {
            if (sale_tx > 0){
                loadAmount = (veh_price-trade_in) * (1 + (sale_tx * 0.01)) - down_pay;
            }else{
                loadAmount = veh_price - down_pay - trade_in;
            }
            var payment = 0;
            if (interest_rate == 0){ //no interest
                payment = loadAmount/nbPayments;
            }else{
                payment = calculatePayment(loadAmount, year, interest_rate);
            }
        }

        $(ctrl.loadAmount).html(loadAmount);
        $(ctrl.montlyPrice).html(payment);
    };

    function calculatePayment(loadAmount, year, interest_rate){
        return calculateInterest(loadAmount, year, interest_rate )
    };

    var calculateInterest = function (total,year,rate) {
       rate = rate / 100 / 12;
       var pow = 1;
       for (var j = 0; j < (year*12); j++)
        pow = pow * (1 + rate);
        
       return 0.01 * Math.round(100*(total * pow * rate) / (pow - 1));
    }

    function clearForm() {
        $(ctrl.vehiculePrice).val("");
        $(ctrl.downPayment).val("");
        $(ctrl.tradeIn).val("");
        $(ctrl.saleTaxe).val("");
        $(ctrl.interestRate).val("");
        $(ctrl.year).val("");        
        $(ctrl.loadAmount).html("0");
        $(ctrl.montlyPrice).html("0");
    };

    return {
        init:init,
        calculate:calculate,
        clearForm:clearForm,
    }
})();